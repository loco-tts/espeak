import commonjs from "@rollup/plugin-commonjs";
import typescript from "@rollup/plugin-typescript";
import copy from 'rollup-plugin-copy';
import dts from "rollup-plugin-dts";
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import packageJson from "./package.json" assert { type: "json" };
export default [
  {
    input: "src/index.ts",
    output: {
      dir: packageJson.main,
      format: "cjs",
      preserveModules: true
    },
    plugins: [
      peerDepsExternal(),
      commonjs(),
      typescript({ tsconfig: "./ts.cjs.release.config.json" }),
      copy({ targets: [
        { src: "src/config/*", dest: `${packageJson.main}/config` },
        { src: "src/voices/*", dest: `${packageJson.main}/voices` }
      ]})
    ],
    external: [ "tslib" ]
  },
  {
    input: "src/index.ts",
    output: {
      dir: packageJson.module,
      format: "esm",
      preserveModules: true
    },
    plugins: [
      peerDepsExternal(),
      commonjs(),
      typescript({ tsconfig: "./ts.esm.release.config.json" }),
      copy({ targets: [
        { src: "src/config/*", dest: `${packageJson.module}/config` },
        { src: "src/voices/*", dest: `${packageJson.module}/voices` }
      ]}),
    ],
    external: [ "tslib" ]
  },
  {
    input: "dist/esm/types/index.d.ts",
    output: [{ file: "dist/index.d.ts", format: "esm" }],
    plugins: [dts()]
  }
];