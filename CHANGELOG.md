# Changelog

## 0.1.0 - 2023-07-10

_First release._

## 0.1.1 - 2023-07-10

### Fixed

- package.json: change `license` from `MIT` to `GPL-3.0-only`

## 0.1.2 - 2023-07-11

### Fixed

- README.md: fix installation instructions, add explicit meSpeak author attribution
- package.json: add LICENSE, package.json, and README.md to `files`

### Changed

- Worker: hardcode web worker script path, remove scriptURL constructor parameter

## 0.2.0 - 2023-08-03

### Fixed

- Worker: fixed web worker initialization by making worker script a module that exports an object with "init" method

### Notes

- Configuration files still need to be copied manually to the public dir from src/config
- Voice files still need to be copied manually to the public dir from src/voices