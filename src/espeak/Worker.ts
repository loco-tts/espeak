import { JOB, PARAMS } from "../constants";
type Job = {
    type: string,
    args: string[],
    resolve?: any,
    reject?: any
};
type JobWithId = Job & { id: number };
const ZERO_INDEX_JOB = {
    type: "NONE",
    args: [],
    id: 0
};
class W extends Worker {
    constructor(workerURL) {
        super(workerURL);
        this.onmessage = event => { this.#jobs[event.data.id].resolve(event.data); };
        this.onerror = event => { console.warn(event); /* TO IMPLEMENT */ };
        console.log(this.#jobs)
    };
    #jobs: JobWithId[] = [ ZERO_INDEX_JOB ];
    #format = (config: any) => (value: number) => {
        const scaled = value * config.SCALE;
        const limited = (scaled < config.MIN) ? config.MIN
            : (scaled > config.MAX) ? config.MAX
            : scaled;
        const rounded = Math.round(limited);
        return [config.FLAG, String(rounded)];
    };
    #fOutput = () => [PARAMS.OUTPUT.FLAG, PARAMS.OUTPUT.FILE];
    #fAmplitude = this.#format(PARAMS.AMPLITUDE);
    #fPitch = this.#format(PARAMS.PITCH);
    #fSpeed = this.#format(PARAMS.SPEED)
    #fEncoding = (value: number) => [PARAMS.ENCODING.FLAG, value];
    #fWordGap = (value: number) => [PARAMS.WORD_GAP.FLAG, value];
    #fVoice = (value: string) => [PARAMS.VOICE.FLAG, value];
    #fPath = () => [PARAMS.PATH.FLAG, PARAMS.PATH.DEFAULT].join("=");
    #composeMessageForLoadConfig(url: string) {
        return ({
            type: JOB.LOAD_CONFIG.TYPE,
            args: [url]
        });
    };
    #composeMessageForLoadVoice(url: string) {
        return ({
            type: JOB.LOAD_VOICE.TYPE,
            args: [url]
        });
    };
    #composeMessageForGenerate(text: string, options: any = {}) {
        const { amplitude, wordGap, pitch, speed, encoding, voice } = options;
        return ({
            type: JOB.SPEAK.TYPE,
            args: ([
                this.#fOutput(),
                this.#fAmplitude(amplitude || PARAMS.AMPLITUDE.DEFAULT),
                this.#fWordGap(wordGap || PARAMS.WORD_GAP.DEFAULT),
                this.#fPitch(pitch || PARAMS.PITCH.DEFAULT),
                this.#fSpeed(speed || PARAMS.SPEED.DEFAULT),
                this.#fEncoding(encoding || PARAMS.ENCODING.DEFAULT),
                this.#fVoice(voice || PARAMS.VOICE.DEFAULT),
                this.#fPath(),
                text
            ] as (string | any)[]).flat()
        });
    };
    async #dispatch(composedMessage: any, transferables: any[] = []) {
        const id = this.#jobs.length;
        this.#jobs.push({ ...composedMessage, id });
        this.postMessage({ ...composedMessage, id }, transferables);
        return await new Promise((resolve, reject) => {
            this.#jobs[id].resolve = resolve;
            this.#jobs[id].reject = reject;
        });
    };
    async loadConfig(url: string) {
        const composedMessage = 
            this.#composeMessageForLoadConfig(url);
        return await this.#dispatch(composedMessage);
    };
    async loadVoice(url: string) {
        const composedMessage = this.#composeMessageForLoadVoice(url);
        return await this.#dispatch(composedMessage);
    };
    async useTransferables() {
        const testTransferable = new ArrayBuffer(1);
        await this.#dispatch(
            { 
                type: JOB.TRANSFERABLES_TEST.TYPE,
                test: testTransferable
            },
            [testTransferable]
        );
        if (!testTransferable?.byteLength) {
            return await this.#dispatch({
                type: JOB.USE_TRANSFERABLES.TYPE,
                args: true
            });
        } else {
            console.warn("Transferables not available. Falling back to copies.");
            return false;
        };
    };
    async generate(text: string, options: any) {
        const composedMessage = this.#composeMessageForGenerate(text, options);
        return await this.#dispatch(composedMessage);
    };
};
export default W;