export const PARAMS = {
    OUTPUT: {
        FLAG: "-w",
        FILE: "wav.wav"
    },
    AMPLITUDE: {
        FLAG: "-a",
        TYPE: "int",
        MIN: 0,
        MAX: 200,
        SCALE: 2,
        DEFAULT: 100
    },
    WORD_GAP: {
        FLAG: "-g",
        MIN: 0,
        MAX: 100,
        SCALE: 1,
        DEFAULT: 0
    },
    PITCH: {
        FLAG: "-p",
        MIN: 0,
        MAX: 99,
        SCALE: 1,
        DEFAULT: 50
    },
    SPEED: {
        FLAG: "-s",
        MIN: 80,
        MAX: 500,
        SCALE: 5,
        DEFAULT: 35
    },
    ENCODING: {
        FLAG: "-b",
        UTF_8: 1,
        UTF_16: 4,
        DEFAULT: 1
    },
    VOICE: {
        FLAG: "-v",
        DEFAULT: "en/en"
    },
    PATH: {
        FLAG: "--path",
        DEFAULT: "/espeak"
    }
};
export const JOB = {
    SPEAK: {
        TYPE: "speak"
    },
    LOAD_CONFIG: {
        TYPE: "loadConfig",
        SUCCESS: "configLoaded"
    },
    LOAD_VOICE: {
        TYPE: "loadVoice"
    },
    USE_TRANSFERABLES: {
        TYPE: "useTransferables"
    },
    TRANSFERABLES_TEST: {
        TYPE: "test"
    }
};
